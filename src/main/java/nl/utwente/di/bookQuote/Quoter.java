package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    private Map<String, Double> prices;

    public Quoter() {
        this.prices = new HashMap<>();
        this.prices.putIfAbsent("1", 10.0);
        this.prices.putIfAbsent("2", 45.0);
        this.prices.putIfAbsent("3", 20.0);
        this.prices.putIfAbsent("4", 35.0);
        this.prices.putIfAbsent("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        Double price = this.prices.get(isbn);
        if (price == null) {
            return 0;
        }
        return price;
    }
}
